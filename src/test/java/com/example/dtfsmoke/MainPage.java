package com.example.dtfsmoke;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class MainPage {

    //Authorization
    @FindBy(xpath  = "//div[@class='navigation-user-login']/descendant::span[text()='�����']")
    public WebElement loginButton;

    @FindBy(xpath = "//button[@class='t-link-inline']/descendant::span[text()='�����']")
    private WebElement authPopUpLoginButton;

    @FindBy(xpath = "//*[contains(@data-gtm,'Login � Mail � Click')]/descendant::span")
    private WebElement authMailButton;

    @FindBy(xpath = "//*[@class = 'v-text-input v-text-input--default']/child::input[@name='login']")
    public WebElement loginField;

    @FindBy(xpath = "//*[@class = 'v-text-input v-text-input--default']/child::input[@name='password']")
    public WebElement PasswordField;

    @FindBy(xpath = "//button[contains(@data-gtm,'Login Mail � Login � Click')]/child::span[contains(@class,'v-button__label')]")
    private WebElement authPopupButtonEnter;

    @FindBy(xpath = "//*[contains(@class,'navigation-user-profile__avatar-image')]/parent::a[contains(@class,'navigation-user-profile__avatar')]")
    private WebElement avatarImageButton;


    //create new Theme
    @FindBy(xpath = "//*[@class = 'navigation-create']/descendant::button[contains(@class , 'v-button v-button--default')]")
    public WebElement createButton;

    @FindBy(xpath = "//*[@class = 'popover-item__main']/descendant::div")
    public WebElement  createThemeButton;

    // Search
    @FindBy(xpath = "//*[contains(@class,'v-text-input v-text-input--default')]/child::input[contains(@class,'v-text-input__input')]")
    private WebElement searchinput;

    //def
    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void authPopupButtonEnterClick (){
        authPopupButtonEnter.click();
    }

    public boolean avatarImageButton () {avatarImageButton.isDisplayed();
        return true;
    }
    public void authMailButtonClick () {authMailButton.click();}

    public void authPopUpLoginButtonClick () {authPopUpLoginButton.click();}


    public void searchinputClick (String search_word) {searchinput.sendKeys(search_word);}


}
