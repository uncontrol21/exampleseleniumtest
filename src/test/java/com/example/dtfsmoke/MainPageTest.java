package com.example.dtfsmoke;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

@ExtendWith(ScreenShotOnFailure.class)
public class MainPageTest   {

    public static WebDriver driver;
   private MainPage mainPage;

    @BeforeAll
    static void setUpAll(){
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.get("https://dtf.ru/");
        mainPage = new MainPage(driver);
        // driver.get(ConfProperties.getProperty("loginPage"));

    }
    @AfterEach
    public void tearDown() {
        driver.quit();
    }

@Test
    public void AuthUser() {

    mainPage.loginButton.click();
    mainPage.authPopUpLoginButtonClick();
    mainPage.authMailButtonClick();

    mainPage.loginField.sendKeys(ConfProperties.getProperty("login"));
    mainPage.PasswordField.sendKeys(ConfProperties.getProperty("password"));

    mainPage.authPopupButtonEnterClick();

    assertTrue(mainPage.avatarImageButton());

    }

    @Test
    public void search() {
        mainPage.searchinputClick(ConfProperties.getProperty("search_word"));
        mainPage.searchinputClick(String.valueOf(Keys.ENTER));
        WebElement searchWord = driver.findElement(By.xpath("//div[@class = 'v-header-title__main']"));
        System.out.println(searchWord.getText());
        Assertions.assertEquals(searchWord.getText(), ConfProperties.getProperty("search_word"));

    }

    @Test
    public void createTheme() {

        //�����������
        mainPage.loginButton.click();
        mainPage.authPopUpLoginButtonClick();
        mainPage.authMailButtonClick();
        mainPage.loginField.sendKeys(ConfProperties.getProperty("login"));
        mainPage.PasswordField.sendKeys(ConfProperties.getProperty("password"));
        mainPage.authPopupButtonEnterClick();
        assertTrue(mainPage.avatarImageButton());

        new WebDriverWait(driver, 1).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@class ='v-popup-window__content']")));
        // �������� ������
        mainPage.createButton.click();
        mainPage.createThemeButton.click();

        WebElement ThemeTitleField = driver.findElement(By.xpath("//textarea[@class = 'writing-title']"));
        ThemeTitleField.sendKeys(ConfProperties.getProperty("titleTestWord"));

        WebElement ThemeContentField = driver.findElement(By.xpath("//div[@class = 'ce-paragraph']"));
        ThemeContentField.sendKeys(ConfProperties.getProperty("contentWords"));

        WebElement Publish_button = driver.findElement(By.xpath("//*[contains(@data-gtm,'Publish Button')]"));
        Publish_button.click();

        WebElement PopUpPublish_button = driver.findElement(By.xpath("//button[contains(@class,'ui-button--2')]/child::span[normalize-space(text()) = '������������']"));
        PopUpPublish_button.click();

        /*
        WebElement notify = driver.findElement(By.xpath("//*[contains(@class,'notify-item--shown notify-item--shown')]/descendant::p[contains(normalize-space(text()),'�����������')]"));
        Assertions.assertTrue(notify.isDisplayed());
*/

    }}
