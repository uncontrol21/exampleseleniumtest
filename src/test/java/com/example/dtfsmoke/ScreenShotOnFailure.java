package com.example.dtfsmoke;
import java.io.File;


import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import static com.example.dtfsmoke.MainPageTest.driver;

public class ScreenShotOnFailure implements AfterTestExecutionCallback {
    @Override
    public void afterTestExecution(ExtensionContext extensionContext) throws Exception {

        Boolean testFailed = extensionContext.getExecutionException().isPresent();
        if (testFailed) {
            System.out.println("����0���");
            File screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenShot, new File("src/test/webapp/screen-captures/" + extensionContext.getDisplayName() + " - " + screenShot.getName()  ));
        }
    }



}
